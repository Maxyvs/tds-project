// ROUTER CONFIGURATION

Router.configure({
    layoutTemplate: 'mainLayout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound',
    waitOn: function() { return [Meteor.subscribe('posts'), Meteor.subscribe('likes'), Meteor.subscribe('comments')]; },
    trackPageView: true
});

// MAIN ROUTES

Router.route('/', function () {
    this.render('index');
});

Router.route('/blog');

Router.route('/contacto', function () {
    this.render('contact');
});

// ACCOUNTS ROUTES

AccountsTemplates.configureRoute('signIn', {
    name: 'signin',
    path: '/login',
    redirect: '/'
});

Router.route('/verify-email', function () {
    this.render('verifyEmail');
});

Router.route('/privacy', function () {
    this.render('notAvailable');
});

Router.route('/terms-of-use', function () {
    this.render('notAvailable');
});

AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('verifyEmail');

// POST ROUTES

Router.route('/posts/add', function () {
    this.render('addPostForm');
});

Router.route('/post/:_id', function () {
    this.render('postPage', {
        data: function () { return Posts.findOne({_id: this.params._id}) }
    });
});

Router.route('/post/edit/:_id', function () {
    this.render('updatePostPage', {
        data: function () { return Posts.findOne({_id: this.params._id}) }
    });
});

// CONTACT FORM

Router.route('/contact-us', function () {
    this.render('contactUsForm');
});
