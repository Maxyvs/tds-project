// SET LANGUAGE

T9n.setLanguage('es');

// ACCOUNTS GLOBAL CONFIGURATION

AccountsTemplates.configure({
    // Behaviour
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: true,
    sendVerificationEmail: true,
    lowercaseUsername: false,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: true,
    showLabels: true,
    showPlaceholders: true,
    showResendVerificationEmailLink: true,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    termsUrl: 'terms-of-use',

    // Redirects
    homeRoutePath: '/',
    redirectTimeout: 4000,

    // Texts
    texts: {
        info: {
            emailSent: "info.emailSent",
            emailVerified: "info.emailVerified",
            pwdChanged: "info.passwordChanged",
            pwdReset: "info.passwordReset",
            pwdSet: "info.passwordReset",
            signUpVerifyEmail: "¡Registro completado! Por favor, consulte su correo electrónico y siga las instrucciones.",
            verificationEmailSent: "Se le ha enviado un correo electrónico. Si la dirección de correo electrónico no aparece en la bandeja de entrada, asegúrese de revisar la carpeta de correo no deseado."
        },
        resendVerificationEmailLink_pre: "¿Has perdido el correo electrónico de confirmación?",
        resendVerificationEmailLink_link: "Enviar de nuevo",
        resendVerificationEmailLink_suff: "",
        title: {
            resendVerificationEmail: "Enviar el correo electrónico de conformación de nuevo"
        },
        button: {
            resendVerificationEmail: "Enviar de nuevo"
        }
    }
});

// FORM FIELDS CONFIGURATION

AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
    {
        _id: 'username',
        type: 'text',
        displayName: 'Nombre de usuario',
        placeholder: 'Nombre de usuario',
        required: true,
        minLength: 5,
        maxLength: 15,
        errStr: 'El nombre de usuario tiene que tener entre 5 y 15 carácteres.'
    },
    {
        _id: 'email',
        type: 'email',
        required: true,
        displayName: "email",
        re: /.+@(.+){2,}\.(.+){2,}/,
        errStr: 'Escribiste una dirección de e-mail no válida.'
    },
    {
        _id: 'password',
        type: 'password',
        required: true,
        displayName: "Contraseña",
        placeholder: "Por lo menos 6 carácteres",
        minLength: 6,
        re: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
        errStr: 'La contraseña debe tener al menos 6 carácteres y dentro de ellos al menos una letra mayúscula, una letra minúscula, un número.'
    }
]);
