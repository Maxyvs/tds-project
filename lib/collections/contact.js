Schema = {};
Schema.contact = new SimpleSchema({
    name: {
        type: String,
        label: "Tu nombre"
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "Dirección de correo electrónico"
    },
    message: {
        type: String,
        label: "Mensaje",
        max: 1000,
        autoform: {
            rows: 10
        }
    }
});