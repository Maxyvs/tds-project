Posts = new Mongo.Collection('posts');

Posts.attachSchema(new SimpleSchema({
    src: {
        type: String,
        label: "Enlace"
    },
    title: {
        type: String,
        label: "Título",
        max: 200
    },
    description: {
        type: String,
        label: "Descripción del post",
        min: 0,
        autoform: {
            rows: 2
        }
    },
    body: {
        type: String,
        label: "Cuerpo del post",
        min: 0,
        autoform: {
            rows: 10
        }
    },
    author: {
        type: String,
        label: "Autor",
        autoform: {
            omit: true
        },
        autoValue: function(){
            return Meteor.users.findOne({_id: this.userId}).username
        }
    },
    createdAt: {
        type: Date,
        label: "Fecha de publicación",
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert) {
                return new Date;
            } else {
                this.unset();
            }
        }
    },
    tag: {
        type: String,
        label: "Tag"
    }
}));
