Meteor.publish('posts', function(postcursor) {
    return Posts.find({},{sort: {createdAt: -1},limit:4, skip:postcursor});
});
Meteor.publish('likes', function() {
    return Likes.find();
});
Meteor.publish('comments', function() {
    return Comments.find();
});
