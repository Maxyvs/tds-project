Meteor.startup(function () {
    process.env.MAIL_URL = 'smtp://postmaster%40sandbox2246d3260a524e25bee6118bdde8f9bb.mailgun.org:f500d268e066114e208d90b5340f8d61@smtp.mailgun.org:587'
    Accounts.emailTemplates.from='no-reply@tdsproyectos.com';
    Accounts.emailTemplates.sitename='TDS Proyectos';

    Accounts.emailTemplates.verifyEmail.subject = function (user) {
        return 'Confirma tu dirección de correo electrónico';
    };
    Accounts.emailTemplates.verifyEmail.text = function (user, url) {
        return 'Copia y pega la dirección URL de la página para confirmar tu dirección de correo electrónico: \n\n' + url;
    }
});
