Meteor.methods({
    sendEmail: function(doc) {
        // Important server-side check for security and data integrity
        check(doc, Schema.contact);

        // Build the e-mail text
        var text = "Nombre: " + doc.name + "\n\n"
            + "Email: " + doc.email + "\n\n\n\n"
            + doc.message;

        this.unblock();

        // Send the e-mail
        Email.send({
            to: "TDSProyectos@hotmail.com",
            from: doc.email,
            subject: "Formulario de contacto de la web - Mensaje de: " + doc.name,
            text: text
        });
    }
});
