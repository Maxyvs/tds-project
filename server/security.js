Posts.permit('insert').ifHasRole('admin').apply();
Posts.permit('remove').ifHasRole('admin').apply();
Posts.permit('update').ifHasRole('admin').apply();
Likes.permit('insert').ifHasRole('admin', 'default-user').apply();
Comments.permit('insert').ifHasRole('admin', 'default-user').apply();
