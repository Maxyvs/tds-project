Template.postsColumn.events({
    'click .loadMore': function (evt, tmpl) {
        Session.set('postCursor', Number(Session.get('postCursor')) +1);
    }
});
Template.post.events({
   'click .like': function (evt, tmpl) {
       var curlike = Likes.findOne({muser:Meteor.userId(),post:tmpl.data._id});
       if(!curlike)
       Likes.insert({muser:Meteor.userId(),post:tmpl.data._id});
   }
});
