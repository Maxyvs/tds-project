AutoForm.hooks({
    insertPostForm: {
        onSuccess: function() {
            Router.go('/blog');
        }
    },
    contactForm: {
        onSuccess: function() {
            Router.go('/');
        }
    },
    updatePostForm: {
        onSuccess: function() {
            Router.go('/blog');
        }
    }
});
