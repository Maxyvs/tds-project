Template.post.helpers({
    numlikes: function () {
        return Likes.find({post: this._id}).count();
    },
    likesthis: function() {
        var doeslike = Likes.findOne({muser:Meteor.userId(),post:this._id});
        if (doeslike)
            return "Te gusta esto";
    },
    commentsCount: function() {
        return Comments.find({postId: this._id}).count();
    }
});
