Template.postsColumn.helpers({
    'posts': function(){
        return Posts.find();
    }
});

Template.post.helpers({
    createdAtFormatted: function () {
        return moment(this.createdAt).format('DD/MM/YYYY');
    }
});

Template.lastPost.helpers({
    createdAtFormatted: function () {
        return moment(this.createdAt).format('DD/MM/YYYY');
    }
});

Template.postPageId.helpers({
    createdAtFormatted: function () {
        return moment(this.createdAt).format('DD/MM/YYYY');
    }
});

Template.lastPosts.helpers({
    'posts': function(){
        return Posts.find({}, {sort: {createdAt: -1}, limit: 4});
    }
});

Template.updatePostForm.helpers({
    beforeRemove: function () {
        return function (collection, id) {
            var doc = collection.findOne(id);
            if (confirm('Estas seguro de que quieres eliminar el post: ' + doc.title +'?')) {
                this.remove();
                Router.go("/blog");
            }
        }
    }
});
